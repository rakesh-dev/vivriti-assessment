# README #

Here is the two assessment app - Todo App and Goodreads App

### Todo App Screenshots: ###

[Todo App](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-09%20at%2010.29.25%20AM.png)

[Test Run](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-09%20at%2010.52.15%20AM.png)

### Get into App] ###

[Get into Todo](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/todo/)

### To Start App ###

* npm install
* npm start

### App Summary ###

* Should be able to add a new Item
* List the existing Items (uncompleted)
* Should be able to edit and delete the existing Item
* Should be able to complete the Item
* List the completed Items

### Test Cases - to run ###

* npm run test

[Test Run](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-09%20at%2010.52.15%20AM.png)

---------------------------------------------------------------------------------------------------------------


### GoodReads App Screenshots: ###

[GoodReads List](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-08%20at%204.28.21%20PM.png)

[GoodReads Search](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-08%20at%204.28.21%20PM.png)

[Test Run](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-09%20at%2010.52.43%20AM%20copy.png)

### Get into App] ###

[Get into GoodReads](https://github.com/vivriticapital/javascript-assignments/tree/master/goodreads-app)

### To Start App ###

* npm install
* npm start

### App Summary ###

* Should be able to search the books with name
* Interface to list the books with pagination

### Test Cases - to run ###

* npm run test

[Test Run](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/Screen%20Shot%202020-06-09%20at%2010.52.43%20AM%20copy.png)


[All Screenshots](https://bitbucket.org/rakesh-dev/vivriti-assessment/src/master/Screeshots/)