import React from 'react';
import { Provider } from 'react-redux'

import './assets/styles/App.css';
import BookListContainer from './containers/BookListContainer'
import SearchFormContainer from './containers/SearchFormContainer'
import mainStore from './store/mainStore'

function App() {
  return (
    <Provider store = {mainStore}>
      <div className="App">
        <div class="card">
          <SearchFormContainer/>
          <BookListContainer/>
        </div>
      </div>
    </Provider>
  );
}

export default App;
