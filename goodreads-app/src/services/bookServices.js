
import { get as _get } from 'lodash'

import { DOMAIN } from './config'
import { URL } from './config'

var parser = require('fast-xml-parser');


class BookService {

  static getBooks() {
		let url = `${DOMAIN}${URL.LIST_BOOKS}`
		let request = new Request(url, { method: 'GET' })
		return fetch(request)
			.then((response) =>  response.text())
				.then((response) => {
					let data;
					var result = parser.validate(response);
					if(result === true) {
						var jsonObj = parser.parse(response);
						data = _get(jsonObj, 'GoodreadsResponse.author.books.book')
					} else {
						data = []
						console.log("error xml",result.err);
					}
					return data
				})
				.catch(error => {
					return error
				});
	}
	
	static searchBooks(payload) {
		let url = `${DOMAIN}${URL.SEARCH_BOOKS}${payload.searchValue}`
		let request = new Request(url, { method: 'GET' })
		return fetch(request)
			.then((response) =>  response.text())
				.then((response) => {
					var result = parser.validate(response)
					if(result === true) {
						var jsonObj = parser.parse(response)
						let data = _get(jsonObj, 'GoodreadsResponse.search.results.work')
						if (data) {
							// search response structure is different from list response
							// manipulating into common structure
							let displayObj = data.map((item) => {
								return {
									title: item.best_book.title,
									small_image_url: item.best_book.small_image_url,
									publication_month: item.original_publication_month,
									publication_year: item.original_publication_year,
									publication_day: item.original_publication_day,
									publisher: item.author ? item.author.name : "",
									average_rating: item.average_rating
								}
							})
							return displayObj
						}
					} else {
						console.log("error xml",result.err)
					}
					return []
				})
				.catch(error => {
					return error
				});
  }

}

export default BookService;