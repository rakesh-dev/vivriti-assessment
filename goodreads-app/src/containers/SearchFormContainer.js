import { connect } from 'react-redux';

import SearchForm from '../components/SearchForm'
import { searchBooks, getBooks } from '../actions/booksAction'


function mapDispatchToProps (dispatch) {
	return {
		searchBooks: (params) => dispatch(searchBooks(params)),
		getBooks: () => dispatch(getBooks())
	}
}

export default connect(null, mapDispatchToProps)(SearchForm)