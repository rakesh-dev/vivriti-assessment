import { connect } from 'react-redux';

import BookList from '../components/BookList'
import { getBooks } from '../actions/booksAction'


function mapDispatchToProps (dispatch) {
	return {
		getBooks: () => dispatch(getBooks())
	}
}

function mapStateToProps (state) {
  return {
    bookList: state.bookReducer
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BookList)