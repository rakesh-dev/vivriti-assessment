import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer';

import BookList from '../BookList'


describe('Render BookList Component', () => {
  let bookList= [
    {
      title:"Tribe",
      small_image_url: "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1580496015l/50731895._SX50_.jpg",
      publisher:  "Timothy",
      publication_year: "<div><b>We All Need Mentors. Here Are More than 100 of the World’s Best.</b>",
      link: "https://www.goodreads.com/book/show/50731895-tribe-of-mentors"
    }
  ]

  it('renders withoud crashing', () => {

    shallow(<BookList bookList={bookList} getBooks={()=> ""}/>)

    const rendered = renderer.create(
      <BookList
        bookList= {bookList}
        getBooks={()=> ""}
      />
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  })

  it('BookList did mount get called properly', () => {
    const getBooks = jest.fn();

    const rendered = renderer.create(
      <BookList
        bookList= {bookList}
        getBooks= {getBooks}
      />
    );
    setTimeout(() => {
      expect(getBooks).toHaveBeenCalled();
      }, 1000);
  })
})