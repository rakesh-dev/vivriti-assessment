import React from 'react'
import { shallow } from 'enzyme'

import SearchFrom from '../SearchForm'

describe('SearchForm component should render properly', () => { 
  it('renders without crashing', () => {
    shallow(<SearchFrom />);
  });

  it('check submit form is working properly', () => {
    const searchBooks = jest.fn();
  
     const wrapper = shallow(
       <SearchFrom
         searchBooks= {searchBooks}
       />
     );
  
     const searchBtn = wrapper.find('#submit-btn');
     searchBtn.simulate('click');
     expect(searchBooks).toHaveBeenCalled();
   });

   it('check reset search is working properly', () => {
    const getBooks = jest.fn();
 
     const wrapper = shallow(
       <SearchFrom
         getBooks= {getBooks}
       />
     );
 
     const searchBtn = wrapper.find('#reset-btn');
     searchBtn.simulate('click');
     expect(getBooks).toHaveBeenCalled();
   });
})