import React from 'react'


class Header extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      searchValue: ''
    }

    this.submitSearchValue = this.submitSearchValue.bind(this)
    this.onChange = this.onChange.bind(this)
    this.resetSearch = this.resetSearch.bind(this)
  }

  submitSearchValue () {
    this.props.searchBooks(this.state)
  }

  onChange (e) {
    this.setState({searchValue: e.target.value})
  }
  
  resetSearch () {
    this.setState({searchValue: ''})
    this.props.getBooks()
  } 
  render () {
    return (
      <div>
        <div class="card-header">
          <div class="row">
            <div class="col-3">
              <p class="h5">Good Reads</p>
            </div>
            <div class="col-4">
              <input type="text" class="form-control" placeholder="Search by title" onChange={this.onChange} value={this.state.searchValue}></input>
            </div>
            <div class="col-4">
              <button type="button" id="submit-btn" class="btn btn-secondary btn-md float-left" onClick={this.submitSearchValue}>Search</button>
              <button type="button" id="reset-btn" class="btn btn-secondary btn-md float-left reset-btn" onClick={this.resetSearch}>Reset Search</button>
            </div>
            <div class="col-2">
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Header;