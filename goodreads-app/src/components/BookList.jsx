import React from 'react'
import Pagination from "react-js-pagination";


class BookList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      currentPage: 1,
      visibleCount: 4
    }

    this.pageChanged = this.pageChanged.bind(this)
  }

  componentDidMount () {
    this.props.getBooks()
  }
  
  pageChanged (selectedPage) {
    this.setState({ currentPage: selectedPage })
  }
  
  render () {
    let { currentPage, visibleCount } = this.state
    let totalItem = this.props.bookList.length
    let emptyList = totalItem === 0 ? <p>No Books Found</p> : ''
    return (
      <div class="container-md card-body">
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="th-sm">Cover Page</th>
              <th class="th-sm">Title</th>
              <th class="th-sm">Published Year</th>
              <th class="th-sm">Publisher</th>
              <th class="th-sm">Rating</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.bookList.length > 0 ? this.props.bookList.slice(currentPage * visibleCount, currentPage * visibleCount + visibleCount).map((item) => 
                <tr>
                  <td><img src={item.small_image_url} class="card-img-top" alt="..."></img></td>
                  <td class="text-left">{item.title}</td>
                  <td>{`${item.publication_year}`}</td>
                  <td class="text-left">{item.publisher}</td>
                  <td>{item.average_rating}</td>
                </tr>
              ) : ''
            }
          </tbody>
        </table>
          <div>{emptyList}</div>
        <div>
          <Pagination
            activePage={currentPage}
            itemsCountPerPage={4}
            totalItemsCount={totalItem}
            pageRangeDisplayed={visibleCount}
            onChange={this.pageChanged}
          />
        </div>
      </div>
    )
  }
}

export default BookList