import BookService from '../services/bookServices'
import { ACTION_TYPE } from '../constants'


export function updateGetBooksSuccess (data) {
  return {
    type: ACTION_TYPE.UPDATE_BOOK_LIST,
    data
  }
}

export function updateSearchBooksSuccess (data) {
  return {
    type: ACTION_TYPE.UPDATE_SEARCH_BOOK_LIST,
    data
  }
}

export function getBooks() {
  return function(dispatch) {
    return BookService.getBooks()
      .then(data => {
        dispatch(updateGetBooksSuccess(data));
      })
  }
}

export function searchBooks (params) {
  return function(dispatch) {
    return BookService.searchBooks(params)
      .then(data => {
        dispatch(updateSearchBooksSuccess(data));
      })
  }
}