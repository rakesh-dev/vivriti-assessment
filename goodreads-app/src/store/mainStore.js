import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux';

import reducer from '../reducers/rootReducer'


let mainStore  = createStore(reducer, applyMiddleware (thunk))

export default mainStore;