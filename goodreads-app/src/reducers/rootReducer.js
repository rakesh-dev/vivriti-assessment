import { combineReducers } from 'redux';

import bookReducer from './bookReducer'


let reducer = combineReducers({bookReducer})

export default reducer;
