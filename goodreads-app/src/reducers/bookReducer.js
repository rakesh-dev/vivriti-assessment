import { ACTION_TYPE } from '../constants'


export default function bookReducer (state=[], action) {
	switch(action.type) {
		case ACTION_TYPE.UPDATE_BOOK_LIST:
		case ACTION_TYPE.UPDATE_SEARCH_BOOK_LIST:
			return action.data
		default:
			return state
		}
}