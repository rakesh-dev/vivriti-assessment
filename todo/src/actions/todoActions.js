import { ACTION_TYPE } from '../constants'


export function addTodoItem (data) {
	return {
		type: ACTION_TYPE.ADD_TODO_ITEM,
		data
	}
}

export function deleteItem (id) {
	return {
		type: ACTION_TYPE.DELETE_TODO_ITEM,
		id
	}
}

export function setAsCompleted (data) {
	return {
		type: ACTION_TYPE.SET_AS_COMPLETED,
		data
	}
}

export function editTodo (data) {
	return {
		type: ACTION_TYPE.EDIT_TODO_ITEM,
		data
	}
}
