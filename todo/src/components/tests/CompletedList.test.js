import React from 'react';
import renderer from 'react-test-renderer';
import CompletedList from '../CompletedList';
import TodoList from '../TodoList';
import { mount, shallow } from 'enzyme';

describe('CompletedList component test add and input value', () => {
  it('renders without crashing', () => {
    shallow(<CompletedList completedList={completedList}/>);
  });

  let completedList = [{
    id: "68a2cd9f4dfcb9f4fd551b62c6b4b96c2f697630",
    isCompleted: false,
    todoSummary: "Need to convert templete to component"
  }]

  it('renders correctly snapshot test', () => {
    const rendered = renderer.create(
      <TodoList
        todoList={completedList}
      />
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });

  it('check edit, delete and complet todo item', () => {
    const editTodoItem = jest.fn();
    const deleteTodoItem = jest.fn();
    const completeTodoItem = jest.fn();

    const wrapper = shallow(
      <CompletedTodo
        deleteItem= {editTodoItem}
        editTodo= {deleteTodoItem}
        setAsCompleted= {completeTodoItem}
        completedList={completedList}
      />)

      const editBtn = wrapper.find('#list-edit-btn');
      editBtn.simulate('click');
      setTimeout(() => {
          const saveBtn = wrapper.find('#list-save-btn');
          saveBtn.simulate('click');
          expect(editTodoItem).toHaveBeenCalled();
        }, 500);
  });

});