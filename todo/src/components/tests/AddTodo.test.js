import React from 'react';
import AddTodo from '../AddTodo';
import { shallow } from 'enzyme';

describe('checking add todo component', () => {
  it('renders without crashing', () => {
    shallow(<AddTodo />);
  });

  it('call add function', () => {
    const addTodoItem = jest.fn();
    const wrapper = shallow(
      <AddTodo addTodoItem={addTodoItem} />
    );

    wrapper.setState({ userInput: "test" });
    const p = wrapper.find('#add-todo');
    p.simulate('click');
    expect(addTodoItem).toHaveBeenCalled();
  });

});