import React from 'react';
import renderer from 'react-test-renderer';
import TodoList from '../TodoList';
import { mount, shallow } from 'enzyme';

describe('CompletedList component test add and input value', () => {
  it('renders without crashing', () => {
    shallow(<TodoList todoList={todoList}/>);
  });

  let todoList = [{
    id: "68a2cd9f4dfcb9f4fd551b62c6b4b96c2f697630",
    isCompleted: false,
    todoSummary: "Need to convert templete to component"
  }]

  it('renders correctly snapshot test', () => {
    const rendered = renderer.create(
      <TodoList
        todoList={todoList}
      />
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });
  
  it('check function edit, delte and complete todo item', () => {
    const editTodoItem = jest.fn();
    const deleteTodoItem = jest.fn();
    const completeTodoItem = jest.fn();

    const wrapper = shallow(
      <TodoList
        todoList={todoList}
        editTodoItem= {editTodoItem}
        deleteTodoItem= {deleteTodoItem}
        completeTodoItem= {completeTodoItem}
      />
    );

    const editBtn = wrapper.find('#list-edit-btn');
    editBtn.simulate('click');
    setTimeout(() => {
        const saveBtn = wrapper.find('#list-save-btn');
        saveBtn.simulate('click');
        expect(editTodoItem).toHaveBeenCalled();
      }, 500);
    })
});