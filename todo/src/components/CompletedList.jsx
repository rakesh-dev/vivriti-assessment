import React from 'react'
import {Pagination} from 'react-bootstrap'

import Item from '../components/Item'


class CompletedList extends React.Component {
  constructor (props) {
    super(props)
    this.setCompleted = this.setCompleted.bind(this)
    this.editTodoSummary = this.editTodoSummary.bind(this)
    this.deleteItem = this.deleteItem.bind(this)
  }

  editTodoSummary (params) {
    this.props.editTodo(params)
  }

  deleteItem (id) {
    this.props.deleteItem(id)
  }

  setCompleted (params) {
    this.props.setAsCompleted(params)
  }

  render () {
    return (
      <div class="container-md border border-padding">
        <div>
          <p class="h5 border-bottom text-left">Completed</p>
        </div>
        {this.props.completedList.map((item) => <Item 
          todo={item} 
          key={item.id} 
          editTodoSummary={this.editTodoSummary}
          deleteItem={this.deleteItem}
          setAsCompleted={this.setCompleted}
          isCompletedList={true}>
          </Item>)
        }
      </div>
    )
  }

}


export default CompletedList;