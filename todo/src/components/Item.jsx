import React from 'react'

class TodoItem extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      itemSummary: '',
      editMode: false
    }

    this.deleteItem = this.deleteItem.bind(this)
    this.setAsCompleted = this.setAsCompleted.bind(this)
    this.setEditMode = this.setEditMode.bind(this)
    this.editTodoSummary = this.editTodoSummary.bind(this)
    this.onSummaryChange = this.onSummaryChange.bind(this)
  }


  editTodoSummary (e) {
    let params = {
      id: e.target.id,
      value: this.state.itemSummary
    }
    this.props.editTodoSummary(params)
    this.setState({ editMode: false })
  }

  deleteItem (e) {
    this.props.deleteItem(e.target.id)
  }

  setAsCompleted (e) {
    let params = {
      id: e.target.id,
      isCompleted: !this.props.todo.isCompleted
    }
    this.props.setAsCompleted(params)
  }

  setEditMode (e) {
    this.setState({ itemSummary: this.props.todo.todoSummary })
    this.setState({ editMode: true })
  }
  
  onSummaryChange (e) {
    this.setState({itemSummary: e.target.value})
  }

  render () {
    let { isCompleted } = this.props.todo
    
    return (
      <div class="container">
        <div class="row border-bottom">
          <div class="col-1 input-group-text">
            <input 
              type="checkbox" 
              id={this.props.todo.id} 
              onChange={this.setAsCompleted} 
              checked={isCompleted}></input>
          </div>
          <div class="col-7 text-left">
            {
              this.state.editMode ? 
              <input 
                type="text" 
                class="form-control" 
                placeholder="todo" 
                onChange={this.onSummaryChange} 
                value={this.state.itemSummary}></input> :
                this.props.isCompletedList ? 
                  <del>{this.props.todo.todoSummary}</del> : 
                  <p>{this.props.todo.todoSummary}</p>
            }
          </div>
          <div class="col-2">
            { 
              this.state.editMode ? 
              <button
                type="button" 
                class="btn btn-light" 
                id={this.props.todo.id}
                onClick={this.editTodoSummary}>Save</button> :
              <button 
                type="button" 
                class="btn btn-light" 
                value={this.props.todo.id}
                onClick={this.setEditMode}>Edit</button>
            }
          </div>
          <div class="col-2">
            <button
              type="button" 
              class="btn btn-light" 
              id={this.props.todo.id} 
              onClick={this.deleteItem}>Delete</button>
          </div>
        </div>
      </div>
    )
  }
}

export default TodoItem