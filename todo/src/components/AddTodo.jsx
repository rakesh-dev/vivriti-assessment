import React from 'react'
import hash from 'object-hash';


class Todo extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
			itemName: ''
		}

		this.onChange = this.onChange.bind(this)
		this.addItem = this.addItem.bind(this)
	}

	addItem (e) {
		this.props.addTodoItem({
			id: hash(this.state.itemName),
			todoSummary: this.state.itemName,
			isCompleted: false
		})
		this.setState({ itemName: '' })
	}

	onChange (e) {
		this.setState({itemName: e.target.value})
	}

	render () {
		return (
			<div class="container-md border border-padding">
				<p class="h5 border-bottom border-bottom-secondary text-left">Add Item</p>
				<div class="row">
					<div class="input-group mb-2 col-9">
						<input 
							type="text" 
							class="form-control" 
							placeholder="Todo Summary" 
							onChange={this.onChange} 
							value={this.state.itemName}></input>
					</div>
					<div class="col-3">
						<button 
							id="add-todo"
							class="btn btn-light" 
							type="button" 
							onClick={this.addItem} 
							disabled={this.state.itemName === ''}>Add</button>
					</div>
				</div>
			</div>
		)
	}
}

export default Todo
