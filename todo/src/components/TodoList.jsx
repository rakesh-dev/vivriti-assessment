import React from 'react'

import Item from '../components/Item'


class TodoList extends React.Component {
  constructor (props) {
    super(props)

    this.setCompleted = this.setCompleted.bind(this)
    this.editTodoSummary = this.editTodoSummary.bind(this)
    this.deleteItem = this.deleteItem.bind(this)
  }

  
  editTodoSummary (params) {
    this.props.editTodo(params)
  }


  deleteItem (id) {
    this.props.deleteItem(id)
  }

  setCompleted (params) {
    this.props.setAsCompleted(params)
  }

  render () {
    return (
      <div class="container-md border border-padding">
        <div>
          <p class="h5 border-bottom text-left">Todo</p>
        </div>
        {this.props.todoList.map((item) => <Item 
          todo={item} 
          key={item.id} 
          editTodoSummary={this.editTodoSummary}
          deleteItem={this.deleteItem}
          setAsCompleted={this.setCompleted}
          isCompletedList={false}>>
          </Item>)}
      </div>
    )
  }
  
}


export default TodoList;