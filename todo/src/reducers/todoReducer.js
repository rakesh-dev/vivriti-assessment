import { ACTION_TYPE } from '../constants'


export default function todoReducer (state=[], action) {
	switch(action.type) {
		case ACTION_TYPE.ADD_TODO_ITEM:
			return [...state, action.data]
		case ACTION_TYPE.DELETE_TODO_ITEM:
			return [...state.filter((item) => (item.id !== action.id))]
		case ACTION_TYPE.SET_AS_COMPLETED:
			return [...state.map((item) => item.id === action.data.id ? 
				{...item, isCompleted: action.data.isCompleted} :
				item)
			]
		case ACTION_TYPE.EDIT_TODO_ITEM:
		return [...state.map((item) => item.id === action.data.id ?
			{...item, todoSummary: action.data.value} :
			item)
		]
		default:
			return state
	}
}