import { combineReducers } from 'redux';
import todoReducer from './todoReducer'


let reducer = combineReducers({todoReducer})

export default reducer;
