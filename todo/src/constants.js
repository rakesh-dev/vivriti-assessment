export const ACTION_TYPE = {
  ADD_TODO_ITEM: 'ADD_TODO_ITEM',
  DELETE_TODO_ITEM: 'DELETE_TODO_ITEM',
  EDIT_TODO_ITEM: 'EDIT_TODO_ITEM',
  SET_AS_COMPLETED: 'SET_AS_COMPLETED'
}