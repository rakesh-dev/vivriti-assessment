import React from 'react';
import { Provider } from 'react-redux'

import './assets/styles/App.css';
import TodoListContainer from './containers/AddTodoContainer'
import AddTodoContainer from './containers/TodoListContainer'
import CompletedListContainer from './containers/CompletedListContainer'
import mainStore from './store/mainStore';


function App () {
  return (
    <Provider store = {mainStore}>
      <div className="App">
        <TodoListContainer/>
        <AddTodoContainer/>
        <CompletedListContainer/>
      </div>
    </Provider>
  );
}

export default App;
