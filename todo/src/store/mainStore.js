import { createStore } from 'redux';

import reducer from '../reducers/rootReducer'


let mainStore  = createStore(reducer)

export default mainStore;