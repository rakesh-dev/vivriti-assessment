import { connect } from 'react-redux';

import CompletedList from '../components/CompletedList'
import { deleteItem, setAsCompleted, editTodo } from '../actions/todoActions'


function mapStateToProps (state) {
	return {
		completedList: state.todoReducer.filter((item) => item.isCompleted === true)
	}
}

function mapDispatchToProps (dispatch) {
	return {
    deleteItem: (params) => dispatch(deleteItem(params)),
    editTodo: (params) => dispatch(editTodo(params)),
    setAsCompleted: (params) => dispatch(setAsCompleted(params))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CompletedList)