import { connect } from 'react-redux';

import AddTodo from '../components/AddTodo'
import { addTodoItem } from '../actions/todoActions'


function mapDispatchToProps (dispatch) {
	return {
		addTodoItem: (params) => dispatch(addTodoItem(params))
	}
}

function mapStateToProps (state) {
	return {
		todoList: state.todoReducer
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo)