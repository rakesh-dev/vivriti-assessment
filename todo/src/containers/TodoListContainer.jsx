import { connect } from 'react-redux';

import TodoList from '../components/TodoList'
import { deleteItem, setAsCompleted, editTodo } from '../actions/todoActions'


function mapStateToProps (state) {
	return {
		todoList: state.todoReducer.filter((item) => item.isCompleted === false)
	}
}

function mapDispatchToProps (dispatch) {
	return {
    deleteItem: (params) => dispatch(deleteItem(params)),
    editTodo: (params) => dispatch(editTodo(params)),
    setAsCompleted: (params) => dispatch(setAsCompleted(params))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)